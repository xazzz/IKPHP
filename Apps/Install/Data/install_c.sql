-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2014 年 04 月 15 日 01:22
-- 服务器版本: 5.6.12-log
-- PHP 版本: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `ikphp`
--

-- --------------------------------------------------------

--
-- 表的结构 `ik_admin`
--

CREATE TABLE IF NOT EXISTS `ik_admin` (
  `userid` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `username` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `role_id` smallint(5) NOT NULL,
  `last_ip` varchar(15) NOT NULL,
  `last_time` int(10) NOT NULL DEFAULT '0',
  `email` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`userid`),
  UNIQUE KEY `user_name` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='管理员' AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- 表的结构 `ik_app`
--

CREATE TABLE IF NOT EXISTS `ik_app` (
  `app_id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) NOT NULL COMMENT 'app名称',
  `app_alias` varchar(255) NOT NULL COMMENT 'app别名',
  `description` text COMMENT '描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:''关闭'',1:开启',
  `host_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0，1',
  `app_entry` varchar(255) DEFAULT NULL COMMENT '前台入口，例：''group/index''',
  `icon_url` varchar(255) DEFAULT NULL COMMENT '小图标地址',
  `large_icon_url` varchar(255) DEFAULT NULL COMMENT '图标地址',
  `admin_entry` varchar(255) DEFAULT NULL COMMENT '后台管理地址',
  `statistics_entry` varchar(255) DEFAULT NULL COMMENT '接口地址',
  `display_order` smallint(5) NOT NULL DEFAULT '0' COMMENT '排序',
  `setuptime` int(11) DEFAULT NULL COMMENT '安装时间戳',
  `version` varchar(255) DEFAULT NULL COMMENT '版本号',
  `api_key` varchar(255) DEFAULT NULL COMMENT '用户api_key',
  `secure_key` varchar(255) DEFAULT NULL COMMENT 'API密钥',
  `author_name` varchar(255) NOT NULL COMMENT '作者',
  `author_url` varchar(255) DEFAULT NULL COMMENT '作者网站',
  `is_nav` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:''否'',1:是',
  `child_menu` text NOT NULL COMMENT '子导航数据',
  PRIMARY KEY (`app_id`),
  KEY `app_name` (`app_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- 转存表中的数据 `ik_app`
--

INSERT INTO `ik_app` (`app_id`, `app_name`, `app_alias`, `description`, `status`, `host_type`, `app_entry`, `icon_url`, `large_icon_url`, `admin_entry`, `statistics_entry`, `display_order`, `setuptime`, `version`, `api_key`, `secure_key`, `author_name`, `author_url`, `is_nav`, `child_menu`) VALUES
(6, 'Group', '小组', '小组是一个大家一起讨论热门话题的地方', 1, 0, 'index/index', './Apps/Group/Appinfo/icon_app.png', './Apps/Group/Appinfo/icon_app_large.png', 'group/admin/index', 'statistics/index', 6, 1397524034, '1.5.4', '', '', '爱客开源', 'http://www.ikphp.com', 1, 'a:0:{}'),
(4, 'Radio', '电台', '豆瓣电台，酷我电台，用户自己添加自己的电台', 1, 0, 'index/index', './Apps/Radio/Appinfo/icon_app.png', './Apps/Radio/Appinfo/icon_app_large.png', 'radio/admin/index', 'statistics/statistics', 6, 1397524022, '1.5', '', '', '爱客开源', 'http://www.ikphp.com', 1, 'a:0:{}'),
(5, 'Article', '阅读', '可以通过投稿、管理员推荐、绑定用户等方式，聚合优质内容，可以采集抓取各大网站内容', 1, 0, 'index/index', './Apps/Article/Appinfo/icon_app.png', './Apps/Article/Appinfo/icon_app_large.png', 'article/admin/index', 'statistics/statistics', 6, 1397524029, '1.5.4', '', '', '爱客开源', 'http://www.ikphp.com', 1, 'a:0:{}');

-- --------------------------------------------------------

--
-- 表的结构 `ik_area`
--

CREATE TABLE IF NOT EXISTS `ik_area` (
  `areaid` int(11) NOT NULL AUTO_INCREMENT,
  `areaname` varchar(32) NOT NULL DEFAULT '',
  `zm` char(1) NOT NULL DEFAULT '' COMMENT '首字母',
  `referid` int(11) NOT NULL DEFAULT '0',
  `ishot` int(1) NOT NULL DEFAULT '0',
  `pinyin` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`areaid`),
  KEY `referid` (`referid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='本地化' AUTO_INCREMENT=202 ;

--
-- 转存表中的数据 `ik_area`
--

INSERT INTO `ik_area` (`areaid`, `areaname`, `zm`, `referid`, `ishot`, `pinyin`) VALUES
(1, '北京', 'B', 0, 1, 'beijing'),
(2, '上海', 'S', 0, 1, 'shanghai'),
(3, '广东', 'G', 0, 0, 'guangdong'),
(4, '江苏', 'J', 0, 0, 'jiangsu'),
(5, '浙江', 'Z', 0, 0, 'zhejiang'),
(6, '山东', 'S', 0, 0, 'shandong'),
(7, '四川', 'S', 0, 0, 'sichuan'),
(8, '湖北', 'H', 0, 0, 'hubei'),
(9, '福建', 'F', 0, 0, 'fujian'),
(10, '河南', 'H', 0, 0, 'henan'),
(11, '辽宁', 'L', 0, 0, 'liaoning'),
(12, '陕西', 'S', 0, 0, 'shanxi'),
(13, '湖南', 'H', 0, 0, 'hunan'),
(14, '河北', 'H', 0, 0, 'hebei'),
(15, '安徽', 'A', 0, 0, 'anhui'),
(16, '黑龙江', 'H', 0, 0, 'heilongjiang'),
(17, '重庆', 'C', 0, 0, 'zhongqing'),
(18, '天津', 'T', 0, 0, 'tianjin'),
(19, '广西', 'G', 0, 0, 'guangxi'),
(20, '山西', 'S', 0, 0, 'shanxi'),
(21, '江西', 'J', 0, 0, 'jiangxi'),
(22, '吉林', 'J', 0, 0, 'jilin'),
(23, '云南', 'Y', 0, 0, 'yunnan'),
(24, '内蒙古', 'N', 0, 0, 'neimenggu'),
(25, '贵州', 'G', 0, 0, 'guizhou'),
(26, '甘肃', 'G', 0, 0, 'gansu'),
(27, '新疆', 'X', 0, 0, 'xinjiang'),
(28, '海南', 'H', 0, 0, 'hainan'),
(29, '宁夏', 'N', 0, 0, 'ningxia'),
(30, '青海', 'Q', 0, 0, 'qinghai'),
(31, '西藏', 'X', 0, 0, 'xicang'),
(32, '香港', 'X', 0, 0, 'xianggang'),
(33, '澳门', 'A', 0, 0, 'aomen'),
(34, '台湾', 'T', 0, 0, 'taiwan'),
(35, '钓鱼岛', 'D', 0, 0, 'diaoyudao'),
(36, '东城区', 'D', 1, 0, 'dongchengqu'),
(37, '西城区', 'X', 1, 0, 'xichengqu'),
(38, '朝阳区', 'C', 1, 0, 'chaoyangqu'),
(39, '丰台区', 'F', 1, 0, 'fengtaiqu'),
(40, '石景山区', 'S', 1, 0, 'shijingshanqu'),
(41, '海淀区', 'H', 1, 0, 'haidianqu'),
(42, '门头沟区', 'M', 1, 0, 'mentougouqu'),
(43, '房山区', 'F', 1, 0, 'fangshanqu'),
(44, '通州区', 'T', 1, 0, 'tongzhouqu'),
(45, '顺义区', 'S', 1, 0, 'shunyiqu'),
(46, '昌平区', 'C', 1, 0, 'changpingqu'),
(47, '大兴区', 'D', 1, 0, 'daxingqu'),
(48, '怀柔区', 'H', 1, 0, 'huairouqu'),
(49, '平谷区', 'P', 1, 0, 'pingguqu'),
(50, '密云县', 'M', 1, 0, 'miyunxian'),
(51, '延庆县', 'Y', 1, 0, 'yanqingxian'),
(52, '地安门', '', 36, 0, ''),
(53, '和平里', '', 36, 0, ''),
(54, '王府井/东单', '', 36, 0, ''),
(55, '建国门/北京站', '', 36, 0, ''),
(56, '东四', '', 36, 0, ''),
(57, '安定门', '', 36, 0, ''),
(58, '朝阳门', '', 36, 0, ''),
(59, '东直门', '', 36, 0, ''),
(60, '广渠门', '', 36, 0, ''),
(61, '左安门', '', 36, 0, ''),
(62, '沙子口', '', 36, 0, ''),
(63, '前门', '', 36, 0, ''),
(64, '崇文门', '', 36, 0, ''),
(65, '天坛', '', 36, 0, ''),
(85, '杨浦区', 'Y', 2, 0, 'yangpuqu'),
(83, '闸北区', 'Z', 2, 0, 'zhabeiqu'),
(82, '普陀区', 'P', 2, 0, 'putuoqu'),
(80, '长宁区', 'C', 2, 0, 'changningqu'),
(79, '徐汇区', 'X', 2, 0, 'xuhuiqu'),
(78, '黄浦区', 'H', 2, 0, 'huangpuqu'),
(84, '虹口区', 'H', 2, 0, 'hongkouqu'),
(81, '静安区', 'J', 2, 0, 'jinganqu'),
(66, '呼和浩特', 'H', 24, 0, 'huhehaote'),
(67, '包头', 'B', 24, 0, 'baotou'),
(68, '呼伦贝尔', 'H', 24, 0, 'hulunbeier'),
(69, '赤峰', 'C', 24, 0, 'chifeng'),
(70, '鄂尔多斯', 'E', 24, 0, 'eerduosi'),
(71, '通辽', 'T', 24, 0, 'tongliao'),
(72, '锡林郭勒', 'X', 24, 0, 'xilinguole'),
(73, '巴彦淖尔', 'B', 24, 0, 'bayannaoer'),
(74, '兴安盟', 'X', 24, 0, 'xinganmeng'),
(75, '乌海', 'W', 24, 0, 'wuhai'),
(76, '乌兰察布', 'W', 24, 0, 'wulanchabu'),
(77, '阿拉善盟', 'A', 24, 0, 'alashanmeng'),
(86, '闵行区', '', 2, 0, 'xingqu'),
(87, '宝山区', 'B', 2, 0, 'baoshanqu'),
(88, '嘉定区', 'J', 2, 0, 'jiadingqu'),
(89, '浦东新区', 'P', 2, 0, 'pudongxinqu'),
(90, '金山区', 'J', 2, 0, 'jinshanqu'),
(91, '松江区', 'S', 2, 0, 'songjiangqu'),
(92, '青浦区', 'Q', 2, 0, 'qingpuqu'),
(93, '奉贤区', 'F', 2, 0, 'fengxianqu'),
(94, '崇明县', 'C', 2, 0, 'chongmingxian'),
(95, '广州', 'G', 3, 1, 'guangzhou'),
(96, '深圳', 'S', 3, 1, 'shen'),
(97, '东莞', 'D', 3, 1, 'dong'),
(98, '佛山', 'F', 3, 0, 'foshan'),
(99, '汕头', 'S', 3, 0, 'shantou'),
(100, '珠海', 'Z', 3, 0, 'zhuhai'),
(101, '惠州', 'H', 3, 0, 'huizhou'),
(102, '中山', 'Z', 3, 0, 'zhongshan'),
(103, '江门', 'J', 3, 0, 'jiangmen'),
(104, '揭阳', 'J', 3, 0, 'jieyang'),
(105, '湛江', 'Z', 3, 0, 'zhanjiang'),
(106, '茂名', 'M', 3, 0, 'maoming'),
(107, '潮州', 'C', 3, 0, 'chaozhou'),
(108, '梅州', 'M', 3, 0, 'meizhou'),
(109, '肇庆', 'Z', 3, 0, 'zhaoqing'),
(110, '韶关', 'S', 3, 0, 'shaoguan'),
(111, '清远', 'Q', 3, 0, 'qingyuan'),
(112, '河源', 'H', 3, 0, 'heyuan'),
(113, '汕尾', 'S', 3, 0, 'shanwei'),
(114, '阳江', 'Y', 3, 0, 'yangjiang'),
(115, '云浮', 'Y', 3, 0, 'yunfu'),
(116, '南京', 'N', 4, 0, 'nanjing'),
(117, '苏州', 'S', 4, 0, 'suzhou'),
(118, '昆山', 'K', 4, 0, 'kunshan'),
(119, '常熟', 'C', 4, 0, 'changshu'),
(120, '张家港', 'Z', 4, 0, 'zhangjiagang'),
(121, '太仓', 'T', 4, 0, 'taicang'),
(122, '无锡', 'W', 4, 0, 'wuxi'),
(123, '江阴', 'J', 4, 0, 'jiangyin'),
(124, '常州', 'C', 4, 0, 'changzhou'),
(125, '徐州', 'X', 4, 0, 'xuzhou'),
(126, '南通', 'N', 4, 0, 'nantong'),
(127, '如皋', 'R', 4, 0, 'rugao'),
(128, '启东', 'Q', 4, 0, 'qidong'),
(129, '扬州', 'Y', 4, 0, 'yangzhou'),
(130, '盐城', 'Y', 4, 0, 'yancheng'),
(131, '连云港', 'L', 4, 0, 'lianyungang'),
(132, '镇江', 'Z', 4, 0, 'zhenjiang'),
(133, '泰州', 'T', 4, 0, 'taizhou'),
(134, '淮安', 'H', 4, 0, 'huaian'),
(135, '宿迁', 'S', 4, 0, 'suqian'),
(136, '杭州', 'H', 5, 1, 'hangzhou'),
(137, '温州', 'W', 5, 0, 'wenzhou'),
(138, '宁波', 'N', 5, 0, 'ningbo'),
(139, '台州', 'T', 5, 0, 'taizhou'),
(140, '金华', 'J', 5, 0, 'jinhua'),
(141, '嘉兴', 'J', 5, 0, 'jiaxing'),
(142, '绍兴', 'S', 5, 0, 'shaoxing'),
(143, '湖州', 'H', 5, 0, 'huzhou'),
(144, '丽水', 'L', 5, 0, 'lishui'),
(145, '衢州', 'Q', 5, 0, 'quzhou'),
(146, '舟山', 'Z', 5, 0, 'zhoushan'),
(147, '青岛', 'Q', 6, 1, 'qingdao'),
(148, '济南', 'J', 6, 0, 'jinan'),
(149, '烟台', 'Y', 6, 0, 'yantai'),
(150, '潍坊', 'W', 6, 0, 'weifang'),
(151, '临沂', 'L', 6, 0, 'linyi'),
(152, '淄博', 'Z', 6, 0, 'zibo'),
(153, '济宁', 'J', 6, 0, 'jining'),
(154, '威海', 'W', 6, 0, 'weihai'),
(155, '泰安', 'T', 6, 0, 'taian'),
(156, '聊城', 'L', 6, 0, 'liaocheng'),
(157, '东营', 'D', 6, 0, 'dongying'),
(158, '枣庄', 'Z', 6, 0, 'zaozhuang'),
(159, '菏泽', 'H', 6, 0, 'heze'),
(160, '日照', 'R', 6, 0, 'rizhao'),
(161, '德州', 'D', 6, 0, 'dezhou'),
(162, '滨州', 'B', 6, 0, 'binzhou'),
(163, '莱芜', 'L', 6, 0, 'laiwu'),
(164, '成都', 'C', 7, 1, 'chengdu'),
(165, '绵阳', 'M', 7, 0, 'mianyang'),
(166, '南充', 'N', 7, 0, 'nanchong'),
(167, '德阳', 'D', 7, 0, 'deyang'),
(168, '达州', 'D', 7, 0, 'dazhou'),
(169, '乐山', 'L', 7, 0, 'leshan'),
(170, '宜宾', 'Y', 7, 0, 'yibin'),
(171, '内江', 'N', 7, 0, 'neijiang'),
(172, '自贡', 'Z', 7, 0, 'zigong'),
(173, '泸州', '', 7, 0, 'zhou'),
(174, '遂宁', 'S', 7, 0, 'suining'),
(175, '广安', 'G', 7, 0, 'guangan'),
(176, '眉山', 'M', 7, 0, 'meishan'),
(177, '广元', 'G', 7, 0, 'guangyuan'),
(178, '攀枝花', 'P', 7, 0, 'panzhihua'),
(179, '资阳', 'Z', 7, 0, 'ziyang'),
(180, '凉山', 'L', 7, 0, 'liangshan'),
(181, '巴中', 'B', 7, 0, 'bazhong'),
(182, '雅安', 'Y', 7, 0, 'yaan'),
(183, '阿坝', 'A', 7, 0, 'aba'),
(184, '甘孜', 'G', 7, 0, 'ganzi'),
(185, '武汉', 'W', 8, 0, 'wuhan'),
(186, '宜昌', 'Y', 8, 0, 'yichang'),
(187, '荆州', 'J', 8, 0, 'jingzhou'),
(188, '襄阳', 'X', 8, 0, 'xiangyang'),
(189, '十堰', 'S', 8, 0, 'shiyan'),
(190, '黄冈', 'H', 8, 0, 'huanggang'),
(191, '黄石', 'H', 8, 0, 'huangshi'),
(192, '孝感', 'X', 8, 0, 'xiaogan'),
(193, '荆门', 'J', 8, 0, 'jingmen'),
(194, '咸宁', 'X', 8, 0, 'xianning'),
(195, '恩施', 'E', 8, 0, 'enshi'),
(196, '随州', 'S', 8, 0, 'suizhou'),
(197, '鄂州', 'E', 8, 0, 'ezhou'),
(198, '仙桃', 'X', 8, 0, 'xiantao'),
(199, '天门', 'T', 8, 0, 'tianmen'),
(200, '潜江', 'Q', 8, 0, 'qianjiang'),
(201, '神农架林区', 'S', 8, 0, 'shennongjialinqu');

-- --------------------------------------------------------

--
-- 表的结构 `ik_article`
--

CREATE TABLE IF NOT EXISTS `ik_article` (
  `aid` int(11) NOT NULL AUTO_INCREMENT COMMENT '文章ID',
  `itemid` int(11) NOT NULL DEFAULT '0' COMMENT '信息ID',
  `content` text NOT NULL COMMENT '内容',
  `postip` varchar(15) NOT NULL DEFAULT '' COMMENT '发布者ip',
  `newsauthor` varchar(20) NOT NULL DEFAULT '' COMMENT '作者',
  `newsfrom` varchar(50) NOT NULL DEFAULT '' COMMENT '来源',
  `newsfromurl` varchar(150) NOT NULL DEFAULT '' COMMENT '来源连接',
  PRIMARY KEY (`aid`),
  KEY `itemid` (`itemid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `ik_article`
--

INSERT INTO `ik_article` (`aid`, `itemid`, `content`, `postip`, `newsauthor`, `newsfrom`, `newsfromurl`) VALUES
(1, 1, '1、阿里巴巴B2B将于11月份关闭日本站\r\n\r\n消息称，阿里巴巴将于今年11月份关闭阿里巴巴日本站，并终止其相关服务。有卖家已收到阿里巴巴官方发给会员的邮件，邮件中显示，由于网站业务调整，阿里巴巴日本站（alibaba.co.jp）计划于2013年11月份左右关闭，并于6月6日在网站首页进行公告。\r\n\r\n[图片1]', '127.0.0.1', '小麦', '', ''),
(2, 2, '详细步骤：\r\n1：苹果2个，白糖80克，苹果味QQ糖30克。\r\n2：苹果去皮切成小块，加入少量清水，放入小锅中煮到变软。\r\n3：放入白糖煮至白糖溶化关火。\r\n4：放凉后，将苹果块放入搅拌机中打成苹果蓉。\r\n5：重新倒入锅里继续煮到适合的浓度，中途需要不停搅拌。\r\n6：放入QQ糖，熬煮至融化即可。\r\n7：煮好后的苹果酱放凉后，装入干净无油的容器即可。\r\n小贴士：\r\n1、自制的苹果酱单独食用或是抹面包、吐司都是不错的选择[图片1]\r\n2、QQ糖主要成份是麦芽糖，用量可多可少适量就好。\r\n\r\n育儿小知识：\r\n【父亲是女孩成功的动力】女孩可能的优势如何转化为真正的优势，父亲在其中发挥着重要作用。父教专家威廉姆?罗纳德认为：“爸爸是一种媒介，通过他，女儿能够理解自己的价值。他所给予女儿的那种毫无条件的认可和赞美，是他的妻子、儿子、母亲、父亲、爱人或者情人，都无法做到的。”', '127.0.0.1', '小麦', '', ''),
(3, 3, 'Marc Hom，丹麦时尚摄影师，出生于1967年，1989年移居纽约，他合作过的时尚品牌有Gucci，Max Mara等，杂志客户包括《名利场》、《VOGUE》、《纽约客》等，作品站点：http://www.serlinassociates.com/artist/marchom。（via）[图片1]', '127.0.0.1', '小麦', '', ''),
(4, 4, '致初恋的一封信致初恋的一封信致初恋的一封信致初恋的一封信致初恋的一封信致初恋的一封信致初恋的一封信致初恋的一封信致初恋的一封信致初恋的一封信致初恋的一封信致初恋的一封信致初恋的一封信致初恋的一封信致初恋的一封信致初恋的一封信致初恋的一封信致初恋的一封信致初恋的一封信致初恋的一封信[图片1]', '127.0.0.1', '小麦', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `ik_article_cate`
--

CREATE TABLE IF NOT EXISTS `ik_article_cate` (
  `cateid` int(11) NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `catename` char(32) NOT NULL DEFAULT '' COMMENT '分类名称',
  `nameid` char(30) NOT NULL DEFAULT '' COMMENT '频道id',
  `orderid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cateid`),
  KEY `nameid` (`nameid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- 转存表中的数据 `ik_article_cate`
--

INSERT INTO `ik_article_cate` (`cateid`, `catename`, `nameid`, `orderid`) VALUES
(10, '互联网', 'technology', 0),
(11, '数码', 'technology', 0),
(12, '美食', 'life', 0),
(13, '居家', 'life', 0),
(14, '时尚', 'life', 0),
(15, '旅游', 'life', 0),
(16, '阅读', 'culture', 0),
(17, '思考', 'culture', 0),
(18, '动漫', 'fun', 0),
(19, '摄影', 'fun', 0),
(20, '情感', 'life', 0);

-- --------------------------------------------------------

--
-- 表的结构 `ik_article_channel`
--

CREATE TABLE IF NOT EXISTS `ik_article_channel` (
  `nameid` char(30) NOT NULL DEFAULT '' COMMENT '频道英文名称',
  `name` char(50) NOT NULL DEFAULT '' COMMENT '频道名',
  PRIMARY KEY (`nameid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文章频道';

--
-- 转存表中的数据 `ik_article_channel`
--

INSERT INTO `ik_article_channel` (`nameid`, `name`) VALUES
('technology', '科技'),
('life', '生活'),
('fun', '趣味'),
('culture', '文化');

-- --------------------------------------------------------

--
-- 表的结构 `ik_article_comment`
--

CREATE TABLE IF NOT EXISTS `ik_article_comment` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) NOT NULL DEFAULT '0' COMMENT '文章ID',
  `referid` int(11) NOT NULL DEFAULT '0' COMMENT '回复ID',
  `userid` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `content` text NOT NULL COMMENT '评论内容',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`cid`),
  KEY `aid` (`aid`),
  KEY `userid` (`userid`),
  KEY `referid` (`referid`,`aid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `ik_article_item`
--

CREATE TABLE IF NOT EXISTS `ik_article_item` (
  `itemid` int(11) NOT NULL AUTO_INCREMENT COMMENT '文章ID',
  `userid` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `cateid` int(11) NOT NULL DEFAULT '0' COMMENT '分类ID',
  `title` char(64) NOT NULL DEFAULT '' COMMENT '标题',
  `count_comment` int(11) NOT NULL DEFAULT '0' COMMENT '回复统计',
  `count_view` int(11) NOT NULL DEFAULT '0' COMMENT '展示数',
  `photoid` int(11) NOT NULL DEFAULT '0' COMMENT '文章主图id',
  `orderid` int(11) NOT NULL DEFAULT '0' COMMENT '排序id',
  `isphoto` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否有图片',
  `isvideo` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否有视频',
  `istop` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否置顶',
  `isshow` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否显示',
  `iscomment` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否允许评论',
  `isdigest` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否精华帖子头条',
  `isaudit` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否审核',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '发布时间',
  `uptime` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`itemid`),
  KEY `userid` (`userid`),
  KEY `cateid` (`cateid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `ik_article_item`
--

INSERT INTO `ik_article_item` (`itemid`, `userid`, `cateid`, `title`, `count_comment`, `count_view`, `photoid`, `orderid`, `isphoto`, `isvideo`, `istop`, `isshow`, `iscomment`, `isdigest`, `isaudit`, `addtime`, `uptime`) VALUES
(1, 1, 10, '派商简讯:价格战天猫升级玩砍价 当当将发4亿代金券', 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1397524389, 0),
(2, 1, 12, '宝宝美食之自制苹果酱', 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1397524459, 0),
(3, 1, 18, '摄影艺术：Marc Hom摄影作品', 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1397524503, 0),
(4, 1, 16, '致初恋的一封信', 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1397524535, 0);

-- --------------------------------------------------------

--
-- 表的结构 `ik_article_robots`
--

CREATE TABLE IF NOT EXISTS `ik_article_robots` (
  `robotid` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '机器人名称',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '添加者id',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加机器人的时间',
  `lasttime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后一次采集时间',
  `importcatid` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '插入的分类ID',
  `robotnum` smallint(6) unsigned NOT NULL DEFAULT '0',
  `listurltype` varchar(10) NOT NULL DEFAULT '' COMMENT '索引列表方式',
  `listurl` text NOT NULL COMMENT '索引列表链接',
  `listpagestart` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '索引列表开始页码',
  `listpageend` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '索引列表结束页码',
  `reverseorder` tinyint(1) NOT NULL DEFAULT '1' COMMENT '索引列表结束页码',
  `allnum` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '总的采集数目',
  `pernum` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '每次采集信息数目',
  `savepic` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否保存信息内的图片',
  `encode` varchar(20) NOT NULL DEFAULT '' COMMENT '采集页面的字符集编码',
  `picurllinkpre` text NOT NULL,
  `saveflash` tinyint(1) NOT NULL DEFAULT '0',
  `subjecturlrule` text NOT NULL,
  `subjecturllinkrule` text NOT NULL,
  `subjecturllinkpre` text NOT NULL,
  `subjectrule` text NOT NULL,
  `subjectfilter` text NOT NULL,
  `subjectreplace` text NOT NULL,
  `subjectreplaceto` text NOT NULL,
  `subjectkey` text NOT NULL,
  `subjectallowrepeat` tinyint(1) NOT NULL DEFAULT '0',
  `datelinerule` text NOT NULL,
  `fromrule` text NOT NULL,
  `authorrule` text NOT NULL,
  `messagerule` text NOT NULL,
  `messagefilter` text NOT NULL,
  `messagepagetype` varchar(10) NOT NULL DEFAULT '',
  `messagepagerule` text NOT NULL,
  `messagepageurlrule` text NOT NULL,
  `messagepageurllinkpre` text NOT NULL,
  `messagereplace` text NOT NULL,
  `messagereplaceto` text NOT NULL,
  `autotype` tinyint(1) NOT NULL DEFAULT '0',
  `wildcardlen` tinyint(1) NOT NULL DEFAULT '0',
  `subjecturllinkcancel` text NOT NULL,
  `subjecturllinkfilter` text NOT NULL,
  `subjecturllinkpf` text NOT NULL,
  `subjectkeycancel` text NOT NULL,
  `messagekey` text NOT NULL,
  `messagekeycancel` text NOT NULL,
  `messageformat` tinyint(1) NOT NULL DEFAULT '0',
  `messagepageurllinkpf` text NOT NULL,
  `uidrule` text NOT NULL COMMENT '发布者UID',
  `defaultaddtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '默认发布时间',
  PRIMARY KEY (`robotid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `ik_comment`
--

CREATE TABLE IF NOT EXISTS `ik_comment` (
  `cid` int(11) NOT NULL AUTO_INCREMENT COMMENT '评论ID',
  `referid` int(11) NOT NULL DEFAULT '0',
  `typeid` int(11) NOT NULL DEFAULT '0' COMMENT '日记ID或帖子id',
  `type` char(64) NOT NULL DEFAULT '0' COMMENT '日记或帖子或其他组件',
  `userid` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `content` text NOT NULL COMMENT '回复内容',
  `addtime` int(11) DEFAULT '0' COMMENT '回复时间',
  PRIMARY KEY (`cid`),
  KEY `typeid` (`typeid`),
  KEY `userid` (`userid`),
  KEY `referid` (`referid`,`typeid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='回复/评论' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `ik_downcount`
--

CREATE TABLE IF NOT EXISTS `ik_downcount` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `userip` char(64) NOT NULL DEFAULT '' COMMENT '下载者ip',
  `downfrom` char(64) NOT NULL DEFAULT '' COMMENT '下载来源',
  `downtime` int(11) NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='统计下载次数' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `ik_feed`
--

CREATE TABLE IF NOT EXISTS `ik_feed` (
  `feedid` int(11) NOT NULL AUTO_INCREMENT COMMENT '动态ID',
  `userid` int(11) NOT NULL COMMENT '用户ID',
  `type` char(50) DEFAULT NULL COMMENT 'feed类型',
  `share_link` varchar(250) DEFAULT NULL COMMENT '分享链接地址',
  `share_name` varchar(250) DEFAULT NULL COMMENT '分享链接标题',
  `topicid` char(50) DEFAULT NULL COMMENT '关联的话题id',
  `addtime` int(11) NOT NULL COMMENT '产生时间戳',
  `is_del` int(2) NOT NULL DEFAULT '0' COMMENT '是否删除 默认为0',
  `count_comment` int(10) DEFAULT '0' COMMENT '评论数',
  `count_repost` int(10) DEFAULT '0' COMMENT '分享数 转发数',
  `is_image` int(2) DEFAULT '0' COMMENT '是否有图片 0-否  1-是',
  `is_repost` int(2) DEFAULT '0' COMMENT '是否转发 0-否  1-是',
  `is_audit` int(2) DEFAULT '1' COMMENT '是否已审核 0-未审核 1-已审核',
  PRIMARY KEY (`feedid`),
  KEY `is_del` (`is_del`,`addtime`),
  KEY `userid` (`userid`,`is_del`,`addtime`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `ik_feed`
--

INSERT INTO `ik_feed` (`feedid`, `userid`, `type`, `share_link`, `share_name`, `topicid`, `addtime`, `is_del`, `count_comment`, `count_repost`, `is_image`, `is_repost`, `is_audit`) VALUES
(1, 1, 'post', '', '', NULL, 1397524603, 0, 0, 0, 1, 0, 1),
(2, 1, 'post', 'http://www.ikphp.com', '爱客网_爱客开源社区程序', NULL, 1397524637, 0, 0, 0, 0, 0, 1),
(3, 1, 'post', '', '', NULL, 1397524674, 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `ik_feed_data`
--

CREATE TABLE IF NOT EXISTS `ik_feed_data` (
  `feedid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '关联feed表，feedid',
  `feeddata` text COMMENT '动态数据，序列化保存',
  `template` text COMMENT '模版内容',
  PRIMARY KEY (`feedid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ik_feed_data`
--

INSERT INTO `ik_feed_data` (`feedid`, `feeddata`, `template`) VALUES
(1, 'a:10:{s:6:"userid";s:1:"1";s:4:"type";s:4:"post";s:10:"share_link";s:0:"";s:10:"share_name";s:0:"";s:7:"topicid";N;s:7:"actname";s:7:" 说：";s:7:"actinfo";s:0:"";s:8:"userinfo";s:68:"<a href="/iksns/index.php?app=space&c=index&a=index&id=1">小麦</a>";s:7:"comment";s:21:"致初恋的一封信";s:11:"attachments";s:325:"<a class="upload-pic-wrapper" href="javascript:;"><img class="upload-pic big" src="/iksns/Data/upload/feed/photo/1/534c8879d4eb6_130_130.jpg?v=1397524603"  small-src="/iksns/Data/upload/feed/photo/1/534c8879d4eb6_130_130.jpg?v=1397524603" data-src="/iksns/Data/upload/feed/photo/1/534c8879d4eb6_500_500.jpg?v=1397524603"></a>";}', '<p class="text">{userinfo}{actname}{actinfo}</p><blockquote><p>{comment}</p></blockquote><div class="attachments">{attachments}</div>'),
(2, 'a:10:{s:6:"userid";s:1:"1";s:4:"type";s:4:"post";s:10:"share_link";s:20:"http://www.ikphp.com";s:10:"share_name";s:34:"爱客网_爱客开源社区程序";s:7:"topicid";N;s:7:"actname";s:15:" 推荐网址  ";s:7:"actinfo";s:83:"<a href="http://www.ikphp.com" target="_top">爱客网_爱客开源社区程序</a>";s:8:"userinfo";s:68:"<a href="/iksns/index.php?app=space&c=index&a=index&id=1">小麦</a>";s:7:"comment";s:63:"输入网址；抓取网址；这个网站是被抓取到得；";s:11:"attachments";s:0:"";}', '<p class="text">{userinfo}{actname}{actinfo}</p><blockquote><p>{comment}</p></blockquote><div class="attachments">{attachments}</div>'),
(3, 'a:10:{s:6:"userid";s:1:"1";s:4:"type";s:4:"post";s:10:"share_link";s:0:"";s:10:"share_name";s:0:"";s:7:"topicid";N;s:7:"actname";s:7:" 说：";s:7:"actinfo";s:0:"";s:8:"userinfo";s:68:"<a href="/iksns/index.php?app=space&c=index&a=index&id=1">小麦</a>";s:7:"comment";s:93:"冬天广播别人可以看见正在发生得；像微博；以后要往微博路上发展！";s:11:"attachments";N;}', '<p class="text">{userinfo}{actname}{actinfo}</p><blockquote><p>{comment}</p></blockquote><div class="attachments">{attachments}</div>');

-- --------------------------------------------------------

--
-- 表的结构 `ik_feed_images`
--

CREATE TABLE IF NOT EXISTS `ik_feed_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `userid` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `feedid` int(11) NOT NULL DEFAULT '0' COMMENT 'feedid',
  `name` char(64) NOT NULL DEFAULT '' COMMENT '文件名',
  `path` char(32) NOT NULL DEFAULT '' COMMENT '源文件路径',
  PRIMARY KEY (`id`),
  KEY `feedid` (`feedid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `ik_feed_images`
--

INSERT INTO `ik_feed_images` (`id`, `userid`, `feedid`, `name`, `path`) VALUES
(1, 1, 1, '534c8879d4eb6.jpg', 'feed/photo/1/');

-- --------------------------------------------------------

--
-- 表的结构 `ik_feed_topic`
--

CREATE TABLE IF NOT EXISTS `ik_feed_topic` (
  `topicid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '话题ID',
  `topicname` varchar(150) NOT NULL COMMENT '话题标题',
  `count_topic` int(11) NOT NULL COMMENT '关联的话题数',
  `addtime` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`topicid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `ik_group`
--

CREATE TABLE IF NOT EXISTS `ik_group` (
  `groupid` int(11) NOT NULL AUTO_INCREMENT COMMENT '小组ID',
  `userid` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `cateid` int(11) NOT NULL DEFAULT '0' COMMENT '分类ID',
  `groupname` char(32) NOT NULL DEFAULT '' COMMENT '群组名字',
  `groupname_en` char(32) NOT NULL DEFAULT '' COMMENT '小组英文名称',
  `groupdesc` text NOT NULL COMMENT '小组介绍',
  `groupicon` char(255) DEFAULT '' COMMENT '小组图标',
  `count_topic` int(11) NOT NULL DEFAULT '0' COMMENT '帖子统计',
  `count_topic_today` int(11) NOT NULL DEFAULT '0' COMMENT '统计今天发帖',
  `count_user` int(11) NOT NULL DEFAULT '0' COMMENT '小组成员数',
  `joinway` tinyint(1) NOT NULL DEFAULT '0' COMMENT '加入方式',
  `role_leader` char(32) NOT NULL DEFAULT '组长' COMMENT '组长角色名称',
  `role_admin` char(32) NOT NULL DEFAULT '管理员' COMMENT '管理员角色名称',
  `role_user` char(32) NOT NULL DEFAULT '成员' COMMENT '成员角色名称',
  `addtime` int(11) DEFAULT '0' COMMENT '创建时间',
  `isrecommend` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否推荐',
  `isopen` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否公开或者私密',
  `isaudit` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否审核',
  `ispost` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否允许会员发帖',
  `isshow` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否显示',
  `uptime` int(11) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  PRIMARY KEY (`groupid`),
  KEY `userid` (`userid`),
  KEY `isshow` (`isshow`),
  KEY `groupname` (`groupname`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `ik_group`
--

INSERT INTO `ik_group` (`groupid`, `userid`, `cateid`, `groupname`, `groupname_en`, `groupdesc`, `groupicon`, `count_topic`, `count_topic_today`, `count_user`, `joinway`, `role_leader`, `role_admin`, `role_user`, `addtime`, `isrecommend`, `isopen`, `isaudit`, `ispost`, `isshow`, `uptime`) VALUES
(1, 1, 59, '爱客官方小组', '', '有好多同学简历上的求职照片是五花八门，真是八仙过海各显神通啊：求职照和本人完全不一样啊！最奇葩的是我曾经通知一姑娘来面试，结果发现他是男的！尼玛！能不能别把你cosplay的照片当求职照啊？虽然星爷的还我漂亮拳能让如花变成秋香，但你是在找工作！能不能靠谱一点？不知道什么样的照片好的话可以问老师、学长或者问度娘啊，随便网上找个帖子也不会犯这种低级错误啊。', 'group/icon/c4ca4238a0b923820dcc509a6f75849b_48_48.jpg', 1, 1, 1, 0, '组长', '管理员', '成员', 1397524130, 1, 0, 0, 0, 0, 1397524144);

-- --------------------------------------------------------

--
-- 表的结构 `ik_group_cate`
--

CREATE TABLE IF NOT EXISTS `ik_group_cate` (
  `cateid` int(11) NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `catename` char(32) NOT NULL DEFAULT '' COMMENT '分类名称',
  `referid` int(11) NOT NULL DEFAULT '0',
  `orderid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cateid`),
  KEY `catename` (`catename`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=76 ;

--
-- 转存表中的数据 `ik_group_cate`
--

INSERT INTO `ik_group_cate` (`cateid`, `catename`, `referid`, `orderid`) VALUES
(1, '兴趣', 0, 0),
(2, '旅行', 1, 0),
(3, '摄影', 1, 0),
(4, '影视', 1, 0),
(5, '音乐', 1, 0),
(6, '文学', 1, 0),
(7, '游戏', 1, 0),
(8, '动漫', 1, 0),
(9, '运动', 1, 0),
(10, '戏曲', 1, 0),
(11, '桌游', 1, 0),
(12, '怪癖', 1, 0),
(13, '生活', 0, 0),
(14, '健康', 13, 0),
(15, '美食', 13, 0),
(16, '宠物', 13, 0),
(17, '美容', 13, 0),
(18, '化妆', 13, 0),
(19, '护肤', 13, 0),
(20, '服饰', 13, 0),
(21, '公益', 13, 0),
(22, '家庭', 13, 0),
(23, '育儿', 13, 0),
(24, '汽车', 13, 0),
(25, '购物', 0, 0),
(26, '淘宝', 25, 0),
(27, '二手', 25, 0),
(28, '团购', 25, 0),
(29, '数码', 25, 0),
(30, '品牌', 25, 0),
(31, '文具', 25, 0),
(32, '社会', 0, 0),
(33, '求职', 32, 0),
(34, '租房', 32, 0),
(35, '励志', 32, 0),
(36, '留学', 32, 0),
(37, '出国', 32, 0),
(38, '理财', 32, 0),
(39, '传媒', 32, 0),
(40, '创业', 32, 0),
(41, '考试', 32, 0),
(42, '艺术', 0, 0),
(43, '设计', 42, 0),
(44, '手工', 42, 0),
(45, '展览', 42, 0),
(46, '曲艺', 42, 0),
(47, '舞蹈', 42, 0),
(48, '雕塑', 42, 0),
(49, '纹身', 42, 0),
(50, '学术', 0, 0),
(51, '人文', 50, 0),
(52, '社科', 50, 0),
(53, '自然', 50, 0),
(54, '建筑', 50, 0),
(55, '国学', 50, 0),
(56, '语言', 50, 0),
(57, '宗教', 50, 0),
(58, '哲学', 50, 0),
(59, '软件', 50, 0),
(60, '硬件', 50, 0),
(61, '互联网', 50, 0),
(62, '情感', 0, 0),
(63, '恋爱', 62, 0),
(64, '心情', 62, 0),
(65, '心理学', 62, 0),
(66, '星座', 62, 0),
(67, '塔罗', 62, 0),
(68, 'LES', 62, 0),
(69, 'GAY', 62, 0),
(70, '闲聊', 0, 0),
(71, '吐槽', 70, 0),
(72, '笑话', 70, 0),
(73, '直播', 70, 0),
(74, '八卦', 70, 0),
(75, '发泄', 70, 0);

-- --------------------------------------------------------

--
-- 表的结构 `ik_group_setting`
--

CREATE TABLE IF NOT EXISTS `ik_group_setting` (
  `name` char(32) NOT NULL DEFAULT '' COMMENT '选项名字',
  `data` char(255) NOT NULL DEFAULT '' COMMENT '选项内容',
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ik_group_setting`
--

INSERT INTO `ik_group_setting` (`name`, `data`) VALUES
('iscreate', '0'),
('group_isaudit', '0'),
('topic_isaudit', '0'),
('maxgroup', '10'),
('jionmax', '50');

-- --------------------------------------------------------

--
-- 表的结构 `ik_group_topics`
--

CREATE TABLE IF NOT EXISTS `ik_group_topics` (
  `topicid` int(11) NOT NULL AUTO_INCREMENT COMMENT '话题ID',
  `typeid` int(11) NOT NULL DEFAULT '0' COMMENT '帖子分类ID',
  `groupid` int(11) NOT NULL DEFAULT '0' COMMENT '小组ID',
  `userid` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `title` char(64) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `count_comment` int(11) NOT NULL DEFAULT '0' COMMENT '回复统计',
  `count_view` int(11) NOT NULL DEFAULT '0' COMMENT '帖子展示数',
  `count_collect` int(11) NOT NULL DEFAULT '0' COMMENT '喜欢收藏数',
  `count_attach` int(11) NOT NULL DEFAULT '0' COMMENT '统计附件',
  `count_recommend` int(11) NOT NULL DEFAULT '0' COMMENT '推荐人数',
  `istop` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否置顶',
  `isshow` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否显示',
  `isaudit` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否审核',
  `iscomment` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否允许评论',
  `isphoto` tinyint(1) NOT NULL DEFAULT '0',
  `isattach` tinyint(1) NOT NULL DEFAULT '0',
  `isnotice` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否通知',
  `isdigest` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否精华帖子',
  `isvideo` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否有视频',
  `addtime` int(11) DEFAULT '0' COMMENT '创建时间',
  `uptime` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`topicid`),
  KEY `groupid` (`groupid`),
  KEY `userid` (`userid`),
  KEY `title` (`title`),
  KEY `groupid_2` (`groupid`,`isshow`),
  KEY `typeid` (`typeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `ik_group_topics`
--

INSERT INTO `ik_group_topics` (`topicid`, `typeid`, `groupid`, `userid`, `title`, `content`, `count_comment`, `count_view`, `count_collect`, `count_attach`, `count_recommend`, `istop`, `isshow`, `isaudit`, `iscomment`, `isphoto`, `isattach`, `isnotice`, `isdigest`, `isvideo`, `addtime`, `uptime`) VALUES
(1, 0, 1, 1, '有好多同学简历上的求职照片是五花八门，真是八仙过海各显神通啊', '[图片1]：求职照和本人完全不一样啊！最奇葩的是我曾经通知一姑娘来面试，结果发现他是男的！尼玛！能不能别把你cosplay的照片当求职照啊？虽然星爷的还我漂亮拳能让如花变成秋香，但你是在找工作！能不能靠谱一点？不知道什么样的照片好的话可以问老师、学长或者问度娘啊，随便网上找个帖子也不会犯这种低级错误啊。', 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1397524144, 1397524165);

-- --------------------------------------------------------

--
-- 表的结构 `ik_group_topics_collects`
--

CREATE TABLE IF NOT EXISTS `ik_group_topics_collects` (
  `userid` int(11) NOT NULL DEFAULT '0',
  `topicid` int(11) NOT NULL DEFAULT '0',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '收藏时间',
  UNIQUE KEY `userid_2` (`userid`,`topicid`),
  KEY `userid` (`userid`),
  KEY `topicid` (`topicid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ik_group_topics_collects`
--

INSERT INTO `ik_group_topics_collects` (`userid`, `topicid`, `addtime`) VALUES
(1, 1, 1397524158);

-- --------------------------------------------------------

--
-- 表的结构 `ik_group_topics_comments`
--

CREATE TABLE IF NOT EXISTS `ik_group_topics_comments` (
  `commentid` int(11) NOT NULL AUTO_INCREMENT COMMENT '评论ID',
  `referid` int(11) NOT NULL DEFAULT '0',
  `topicid` int(11) NOT NULL DEFAULT '0' COMMENT '话题ID',
  `userid` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `content` text NOT NULL COMMENT '回复内容',
  `addtime` int(11) DEFAULT '0' COMMENT '回复时间',
  PRIMARY KEY (`commentid`),
  KEY `topicid` (`topicid`),
  KEY `userid` (`userid`),
  KEY `referid` (`referid`,`topicid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `ik_group_topics_comments`
--

INSERT INTO `ik_group_topics_comments` (`commentid`, `referid`, `topicid`, `userid`, `content`, `addtime`) VALUES
(1, 0, 1, 1, '有好多同学简历上的求职照片是五花八门，真是八仙过海各显神通啊', 1397524165);

-- --------------------------------------------------------

--
-- 表的结构 `ik_group_topics_recommend`
--

CREATE TABLE IF NOT EXISTS `ik_group_topics_recommend` (
  `userid` int(11) NOT NULL DEFAULT '0',
  `topicid` int(11) NOT NULL DEFAULT '0',
  `content` char(250) NOT NULL DEFAULT '',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '推荐时间',
  UNIQUE KEY `userid_2` (`userid`,`topicid`),
  KEY `userid` (`userid`),
  KEY `topicid` (`topicid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ik_group_topics_recommend`
--

INSERT INTO `ik_group_topics_recommend` (`userid`, `topicid`, `content`, `addtime`) VALUES
(1, 1, '有好多同学简历上的求职照片是五花八门，真是八仙过海各显神通啊', 1397524162);

-- --------------------------------------------------------

--
-- 表的结构 `ik_group_users`
--

CREATE TABLE IF NOT EXISTS `ik_group_users` (
  `userid` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `groupid` int(11) NOT NULL DEFAULT '0' COMMENT '群组ID',
  `isadmin` int(11) NOT NULL DEFAULT '0' COMMENT '是否管理员',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '加入时间',
  UNIQUE KEY `userid_2` (`userid`,`groupid`),
  KEY `userid` (`userid`),
  KEY `groupid` (`groupid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ik_group_users`
--

INSERT INTO `ik_group_users` (`userid`, `groupid`, `isadmin`, `addtime`) VALUES
(1, 1, 0, 1397524130);

-- --------------------------------------------------------

--
-- 表的结构 `ik_home_info`
--

CREATE TABLE IF NOT EXISTS `ik_home_info` (
  `infoid` int(11) NOT NULL AUTO_INCREMENT,
  `infokey` char(32) NOT NULL DEFAULT '',
  `infocontent` text NOT NULL,
  PRIMARY KEY (`infoid`),
  UNIQUE KEY `infokey` (`infokey`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `ik_home_info`
--

INSERT INTO `ik_home_info` (`infoid`, `infokey`, `infocontent`) VALUES
(1, 'about', '<h2>爱客网（IKPHP.COM）</h2><p  style="margin:5px 0px">爱客网是开放、多元的泛科技兴趣社区，并提供负责任、有智趣的科技内容。你可以在这里：</p> <ul style="margin:5px 0px"><li style="list-style: disc inside none;">依兴趣关注不同的小站和小组，阅读有意思的科技内容；</li> <li style="list-style: disc inside none;">在"爱客问答"里提出困惑你的科技问题，或提供靠谱的答案；</li> <li style="list-style: disc inside none;">关注各个门类和领域的爱客达人，加入兴趣小组讨论，分享智趣话题。</li> </ul>      <p  style="margin:5px 0px">爱客网的创始人是小麦，他是一位IT爱好者；热衷于PHP和前端开发，经过不懈的努力和追求；他在不断的完善爱客网；为广大爱好互联网科技者提供点点贡献。</p><p  style="margin:5px 0px">爱客网(IKPHH)社区将不断完善社区系统的建设，以简单和高扩展的形式为用户提供各种不同功能的社区应用，爱客网(IKPHH)开源社区将不断满足用户对社区建设和运营等方面的需求。</p><p  style="margin:5px 0px">爱客网是一个非盈利性个人网站， 它是在不违背社会主义道德底线的公益网站！它有着和其他社区同仁一样的激情！</p><p  style="margin:5px 0px">官方网站：<a href="http://www.ikphp.com/">http://www.ikphp.com</a></p>'),
(2, 'contact', '<p>Email:160780470#qq.com(#换@)</p>\r\n<p>QQ号:160780470</p>\r\n<p>QQ群:141611512 、288584398</p>\r\n<p>Location:北京 朝阳区 </p>'),
(3, 'agreement', '<p>1、爱客网(IKPHP)开源社区免费开源</p>\r\n<p>2、你可以免费使用爱客网(IKPHP)开源社区</p>\r\n<p>3、你可以在爱客网(IKPHP)开源社区基础上进行二次开发和修改</p>\r\n<p>4、你可以拿爱客网(IKPHP)开源社区建设你的商业运营网站</p>\r\n\r\n<p>5、在爱客网(IKPHP)开源社区未进行商业运作之前，爱客网(IKPHP)开源社区(小麦)将拥有对爱客网(IKPHP)开源社区的所有权，任何个人，公司和组织不得以任何形式和目的侵犯爱客网(IKPHP)开源社区的版权和著作权</p>\r\n<p>6、爱客网(IKPHP)开源社区拥有对此协议的修改和不断完善。</p>'),
(4, 'privacy', '<p>爱客网(IKPHP)开源社区（ikphp.com）以此声明对本站用户隐私保护的许诺。爱客网(IKPHP)开源社区的隐私声明正在不断改进中，随着本站服务范围的扩大，会随时更新隐私声明。我们欢迎你随时查看隐私声明。</p>');

-- --------------------------------------------------------

--
-- 表的结构 `ik_images`
--

CREATE TABLE IF NOT EXISTS `ik_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `seqid` int(11) NOT NULL DEFAULT '0' COMMENT 'seqid',
  `typeid` int(11) NOT NULL DEFAULT '0' COMMENT '日记ID或帖子id',
  `type` char(64) NOT NULL DEFAULT '0' COMMENT '日记或帖子或其他组件',
  `userid` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `name` char(64) NOT NULL DEFAULT '' COMMENT '文件名',
  `path` char(32) NOT NULL DEFAULT '' COMMENT '源文件路径',
  `size` char(32) NOT NULL DEFAULT '',
  `title` char(120) NOT NULL DEFAULT '' COMMENT '图片描述',
  `align` char(32) NOT NULL DEFAULT 'C' COMMENT '图片对齐方式',
  `addtime` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `typeid` (`typeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- 转存表中的数据 `ik_images`
--

INSERT INTO `ik_images` (`id`, `seqid`, `typeid`, `type`, `userid`, `name`, `path`, `size`, `title`, `align`, `addtime`) VALUES
(1, 1, 1, 'note', 1, '534c8487665fc.jpg', 'note/2014/0415/08/', '197503', 'c9fcc3cec3fdfc037a4a34b4d63f8794a4c22688.jpg', 'C', 1397523591),
(2, 1, 2, 'note', 1, '534c84a999e08.jpg', 'note/2014/0415/09/', '379617', '123.jpg', 'C', 1397523626),
(3, 1, 3, 'note', 1, '534c84c983211.jpg', 'note/2014/0415/09/', '152244', '4ec2d5628535e5dd3cd03bde74c6a7efce1b6237.jpg', 'C', 1397523658),
(4, 1, 4, 'note', 1, '534c84e500e5c.jpg', 'note/2014/0415/09/', '100605', 'ac4bd11373f0820219e90e3e49fbfbedab641bb3.jpg', 'C', 1397523685),
(5, 1, 1, 'topic', 1, '534c86af7817c.jpg', 'topic/2014/0415/09/', '181418', '', 'C', 1397524144),
(6, 1, 1, 'article', 1, '534c87a3e3441.jpg', 'article/2014/0415/09/', '14124', '', 'C', 1397524387),
(7, 1, 2, 'article', 1, '534c87ea66079.jpg', 'article/2014/0415/09/', '15290', '', 'C', 1397524458),
(8, 1, 3, 'article', 1, '534c8816d1957.jpg', 'article/2014/0415/09/', '31970', '', 'C', 1397524503),
(9, 1, 4, 'article', 1, '534c8835b4f65.jpg', 'article/2014/0415/09/', '23345', '', 'C', 1397524533);

-- --------------------------------------------------------

--
-- 表的结构 `ik_message`
--

CREATE TABLE IF NOT EXISTS `ik_message` (
  `messageid` int(11) NOT NULL AUTO_INCREMENT COMMENT '消息ID',
  `userid` int(11) NOT NULL DEFAULT '0' COMMENT '发送用户ID',
  `touserid` int(11) NOT NULL DEFAULT '0' COMMENT '接收消息的用户ID',
  `title` char(64) NOT NULL DEFAULT '' COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `isread` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已读',
  `isspam` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否垃圾邮件',
  `isinbox` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否在收件箱显示',
  `isoutbox` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否在发件箱显示',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`messageid`),
  KEY `touserid` (`touserid`,`isread`),
  KEY `userid` (`userid`,`touserid`,`isread`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='短消息表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `ik_oauth`
--

CREATE TABLE IF NOT EXISTS `ik_oauth` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `config` text NOT NULL,
  `desc` text NOT NULL,
  `author` varchar(50) NOT NULL,
  `ordid` tinyint(3) unsigned NOT NULL DEFAULT '255',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `ik_oauth`
--

INSERT INTO `ik_oauth` (`id`, `code`, `name`, `config`, `desc`, `author`, `ordid`, `status`) VALUES
(1, 'qq', 'QQ登录', 'a:2:{s:7:"app_key";s:9:"100401235";s:10:"app_secret";s:32:"567e145f267ccde6694acb2c2582cf42";}', '申请地址：http://connect.opensns.qq.com/', 'IKPHP TEAM', 1, 1),
(2, 'sina', '微博登陆', 'a:2:{s:7:"app_key";s:10:"1001094537";s:10:"app_secret";s:32:"4228d0fe4e7000c37aad1727c1cca385";}', '申请地址：http://open.weibo.com/', 'IKPHP TEAM', 2, 1);

-- --------------------------------------------------------

--
-- 表的结构 `ik_setting`
--

CREATE TABLE IF NOT EXISTS `ik_setting` (
  `name` char(32) NOT NULL DEFAULT '' COMMENT '选项名字',
  `data` varchar(500) NOT NULL DEFAULT '' COMMENT '选项内容',
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='系统管理配置';

--
-- 转存表中的数据 `ik_setting`
--

INSERT INTO `ik_setting` (`name`, `data`) VALUES
('site_title', '爱客网'),
('site_subtitle', '又一个爱客开源社区'),
('site_url', 'http://127.0.0.1/iksns/'),
('site_email', 'admin@admin.com'),
('site_icp', '京ICP备13018602号'),
('site_keywords', 'IKPHP'),
('site_desc', '又一个爱客网(IKPHP)开源社区'),
('site_theme', 'blue'),
('isgzip', '0'),
('timezone', '8'),
('isinvite', '0'),
('charset', 'UTF-8'),
('integrate_code', 'default'),
('integrate_config', ''),
('avatar_size', '24,32,48,64,100,160,200'),
('attr_allow_exts', 'jpg,gif,png,jpeg'),
('attr_allow_size', '2048'),
('attach_path', 'Data/upload/'),
('simg', 'a:2:{s:5:"width";s:3:"170";s:6:"height";s:3:"170";}'),
('mimg', 'a:2:{s:5:"width";s:3:"600";s:6:"height";s:3:"600";}'),
('bimg', 'a:2:{s:5:"width";s:4:"1000";s:6:"height";s:4:"1000";}'),
('mailhost', 'smtp.qq.com'),
('mailport', '25'),
('mailuser', 'test'),
('mailpwd', 'test');

-- --------------------------------------------------------

--
-- 表的结构 `ik_tag`
--

CREATE TABLE IF NOT EXISTS `ik_tag` (
  `tagid` int(11) NOT NULL AUTO_INCREMENT,
  `tagname` char(16) NOT NULL DEFAULT '',
  `count_user` int(11) NOT NULL DEFAULT '0',
  `count_group` int(11) NOT NULL DEFAULT '0',
  `count_topic` int(11) NOT NULL DEFAULT '0',
  `count_bang` int(11) NOT NULL DEFAULT '0',
  `count_article` int(11) NOT NULL DEFAULT '0',
  `count_note` int(11) NOT NULL DEFAULT '0',
  `count_site` int(11) NOT NULL DEFAULT '0',
  `isenable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否可用',
  `uptime` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`tagid`),
  UNIQUE KEY `tagname` (`tagname`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `ik_tag`
--

INSERT INTO `ik_tag` (`tagid`, `tagname`, `count_user`, `count_group`, `count_topic`, `count_bang`, `count_article`, `count_note`, `count_site`, `isenable`, `uptime`) VALUES
(1, '生活', 0, 1, 0, 0, 0, 0, 0, 0, 1397524130),
(2, '同城', 0, 1, 0, 0, 0, 0, 0, 0, 1397524130),
(3, '影视', 0, 1, 0, 0, 0, 0, 0, 0, 1397524130),
(4, '工作室', 0, 1, 0, 0, 0, 0, 0, 0, 1397524130),
(5, '艺术', 0, 1, 0, 0, 0, 0, 0, 0, 1397524130);

-- --------------------------------------------------------

--
-- 表的结构 `ik_tag_group_index`
--

CREATE TABLE IF NOT EXISTS `ik_tag_group_index` (
  `groupid` int(11) NOT NULL DEFAULT '0',
  `tagid` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `groupid_2` (`groupid`,`tagid`),
  KEY `groupid` (`groupid`),
  KEY `tagid` (`tagid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ik_tag_group_index`
--

INSERT INTO `ik_tag_group_index` (`groupid`, `tagid`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5);

-- --------------------------------------------------------

--
-- 表的结构 `ik_tag_topic_index`
--

CREATE TABLE IF NOT EXISTS `ik_tag_topic_index` (
  `topicid` int(11) NOT NULL DEFAULT '0' COMMENT '帖子ID',
  `tagid` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `topicid_2` (`topicid`,`tagid`),
  KEY `topicid` (`topicid`),
  KEY `tagid` (`tagid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ik_ucenter_user`
--

CREATE TABLE IF NOT EXISTS `ik_ucenter_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `username` char(16) NOT NULL COMMENT '用户名',
  `password` char(32) NOT NULL COMMENT '密码',
  `email` char(32) NOT NULL COMMENT '用户邮箱',
  `doname` char(32) NOT NULL DEFAULT '' COMMENT '用户个性域名',
  `mobile` char(15) NOT NULL COMMENT '用户手机',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '注册IP',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `last_login_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后登录IP',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) DEFAULT '0' COMMENT '是否启用：0启用1禁用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='UC用户表' AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `ik_ucenter_user`
--

INSERT INTO `ik_ucenter_user` (`id`, `username`, `password`, `email`, `doname`, `mobile`, `reg_time`, `reg_ip`, `last_login_time`, `last_login_ip`, `update_time`, `status`) VALUES
(1, '小麦', 'd9916b1117b9b366d7bba97bd6cb9a51', 'ikphp@sina.cn', '', '', 1397523536, 2130706433, 1397523548, 2130706433, 1397523536, 0);

-- --------------------------------------------------------

--
-- 表的结构 `ik_user`
--

CREATE TABLE IF NOT EXISTS `ik_user` (
  `userid` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `username` char(16) NOT NULL COMMENT '用户名',
  `email` char(32) NOT NULL COMMENT '用户邮箱',
  `doname` char(32) NOT NULL DEFAULT '' COMMENT '用户个性域名',
  `fuserid` int(11) NOT NULL DEFAULT '0' COMMENT '来自邀请用户',
  `sex` tinyint(1) NOT NULL DEFAULT '0' COMMENT '性别',
  `phone` char(16) NOT NULL DEFAULT '' COMMENT '电话号码',
  `roleid` int(11) NOT NULL DEFAULT '1' COMMENT '角色ID',
  `areaid` int(11) NOT NULL DEFAULT '0' COMMENT '区县ID',
  `path` char(32) NOT NULL DEFAULT '' COMMENT '头像路径',
  `face` char(64) NOT NULL DEFAULT '' COMMENT '会员头像',
  `signed` char(64) NOT NULL DEFAULT '' COMMENT '签名',
  `blog` char(32) NOT NULL DEFAULT '' COMMENT '博客',
  `about` char(255) NOT NULL DEFAULT '' COMMENT '关于我',
  `last_login_ip` varchar(16) NOT NULL DEFAULT '' COMMENT '登陆IP',
  `address` char(64) NOT NULL DEFAULT '',
  `qq_openid` char(32) NOT NULL DEFAULT '',
  `qq_access_token` char(32) NOT NULL DEFAULT '' COMMENT 'access_token',
  `count_login` int(11) NOT NULL DEFAULT '0' COMMENT '统计登录次数',
  `count_score` int(11) NOT NULL DEFAULT '0' COMMENT '统计积分',
  `count_follow` int(11) NOT NULL DEFAULT '0' COMMENT '统计用户跟随的',
  `count_followed` int(11) NOT NULL DEFAULT '0' COMMENT '统计用户被跟随的',
  `isadmin` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否是管理员',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否启用：0启用1禁用',
  `isverify` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未验证1验证',
  `verifycode` char(11) NOT NULL DEFAULT '' COMMENT '验证码',
  `resetpwd` char(32) NOT NULL DEFAULT '' COMMENT '重设密码',
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '注册IP',
  `reg_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `last_login_time` int(11) DEFAULT '0' COMMENT '最后登陆时间',
  PRIMARY KEY (`userid`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户表' AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `ik_user`
--

INSERT INTO `ik_user` (`userid`, `username`, `email`, `doname`, `fuserid`, `sex`, `phone`, `roleid`, `areaid`, `path`, `face`, `signed`, `blog`, `about`, `last_login_ip`, `address`, `qq_openid`, `qq_access_token`, `count_login`, `count_score`, `count_follow`, `count_followed`, `isadmin`, `status`, `isverify`, `verifycode`, `resetpwd`, `reg_ip`, `reg_time`, `last_login_time`) VALUES
(1, '小麦', 'ikphp@sina.cn', '', 0, 0, '', 1, 0, '', '', '', '', '', '2130706433', '', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', 2130706433, 1397523548, 1397523548);

-- --------------------------------------------------------

--
-- 表的结构 `ik_user_bind`
--

CREATE TABLE IF NOT EXISTS `ik_user_bind` (
  `uid` int(10) unsigned NOT NULL,
  `type` varchar(60) NOT NULL,
  `keyid` varchar(100) NOT NULL,
  `info` text NOT NULL,
  KEY `uid` (`uid`),
  KEY `uid_type` (`uid`,`type`),
  KEY `type_keyid` (`type`,`keyid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ik_user_follow`
--

CREATE TABLE IF NOT EXISTS `ik_user_follow` (
  `userid` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `userid_follow` int(11) NOT NULL DEFAULT '0' COMMENT '被关注的用户ID',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  UNIQUE KEY `userid_2` (`userid`,`userid_follow`),
  KEY `userid` (`userid`),
  KEY `userid_follow` (`userid_follow`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户关注跟随';

-- --------------------------------------------------------

--
-- 表的结构 `ik_user_note`
--

CREATE TABLE IF NOT EXISTS `ik_user_note` (
  `noteid` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志ID',
  `userid` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `title` char(64) NOT NULL DEFAULT '' COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `count_comment` int(11) NOT NULL DEFAULT '0' COMMENT '回复统计',
  `count_view` int(11) NOT NULL DEFAULT '0' COMMENT '展示数',
  `privacy` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否可见 1:所有人可见 2仅好友 3仅自己可见',
  `isaudit` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否审核 1是 0否',
  `isreply` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否可以回应 1是 0否',
  `isrecommend` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否推荐 1是 0否',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`noteid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `ik_user_note`
--

INSERT INTO `ik_user_note` (`noteid`, `userid`, `title`, `content`, `count_comment`, `count_view`, `privacy`, `isaudit`, `isreply`, `isrecommend`, `addtime`) VALUES
(1, 1, '我是球迷，经常看欧洲的足球视频', '非日记，私人存歌贴，非极品不存，当自己的豆瓣FM了，顺便分析给友邻们\r\n每句每字都听不懂，不过真的好听\r\n最贱的时候，连越南天团htk我都听\r\n\r\n[图片1]\r\n偶尔听到好听的背景视频，感觉很好听，但是没有存下名字，过一阵想听却找不到\r\n自己开个日记，整理一下小语种的，免得再有一首歌找好几年的现象\r\n主要有：德语 法语 西班牙语 俄语\r\n其次：葡萄牙语 意大利语 丹麦语 罗马尼亚语等\r\n\r\n先是这几年的存的，不定时更新\r\n还有一些歌忘了，或者一直找不到名字\r\n欢迎补充推荐\r\n====================================================================\r\n目录：\r\n一：适合循环播放的男女对唱系列\r\n二：法语\r\n三：德语\r\n四：西班牙语（含拉美）\r\n五：俄语（含东欧）\r\n六：意大利语\r\n七：丹麦语\r\n八：葡萄牙语\r\n九：其他语种\r\n十：英语冷门好听\r\n-------------------------------------------------------------------------------------------------------------------------------------------\r\n超级极品先推荐：\r\n1.\r\n歌名值100元的优酷视频：\r\n在优酷上，一个无名的音乐视频里面，很多人求歌名，找了3年之久，有个人100元求这个歌名', 0, 0, 1, 1, 0, 1, 1397523595),
(2, 1, '摔之前是穿着一条西裤，站起来就剩一只西裤，一个裤腿摔碎了。', '就是今天和朋友出去逛街，路上有英语培训机构的兼职人员，让我们写个名字和电话，说有任务。\r\n我和朋友照办，这又不是什么大事。\r\n下午就接到电话，一个女生打的，邀请我去中关村附近参加一个英语培训的免费活动。\r\n我说“我出差了，机会给别人吧。”\r\n这是我一贯的用词，考虑到他们也不容易，一个善意的谎言。\r\n她说“不对啊，你不是上午填表申请了么？怎么能出差呢？”\r\n我说“上午还没出差。”\r\n她说“那既然要出差，干嘛还申请呢？”\r\n我说“公司突然通知。”\r\n她说“什么时候走，去哪啊。”\r\n我随便瞎编，说“明天，去天津。”\r\n她说“那没关系，我们晚上的活动，不影响的。”\r\n我说“不行啊，我得准备行李。”\r\n她说“天津好近的啊，还用收拾么？”\r\n我说“你怎么这么认真，我不想去。”\r\n她说“不想去干嘛填表。”\r\n我说“你们的人求我帮忙。”\r\n她说“帮什么忙？你不参加活动还能帮什么忙？”\r\n我说“他让我留电话，说一天有多少电话的任务。”\r\n她说“他们就这样，总说留个电话就行。你说那能行么？嗯？”\r\n我说“嗯。”\r\n她一声叹息“唉。”\r\n我不说话，等她挂机。\r\n她说“让你留电话你就答应他，这种对我们没意义啊。”\r\n我说“学生兼职不容易，帮一下呗。”\r\n她说“想帮你就来参加活动。嗯？”\r\n我说“不去。”\r\n她说“不来你还敢说帮。”\r\n我说“行行行，我错了。”\r\n她说“唉，以后别再这样了，好么？”\r\n我说“好。”\r\n她说“为了礼貌，请你先挂机吧。”\r\n我说“再见。”\r\n\r\n我只是觉得有意思，还有这么执着的电销呢。\r\n想起一些乱七八糟的关于推销的事情，乱侃侃。\r\n现在的电销真是绝了，天天中奖，不是让我去西单取钱，就是去西直门取金条。\r\n最逗的是，怎么都问“您的尾号是XXXX么？”\r\n你都打通了，你说我是不是？\r\n\r\n前几天还保险公司打电话，说马航都失事了，快买保险吧。\r\n这根就不能触动我，本人有恐飞症，恐高症。\r\n以前陪朋友坐摩天轮，到最高处的时候，像外面看了一眼，差点没被背过气去。\r\n那摩天轮质量根本就是，转的时候还吱吱的响，要命了。\r\n主要是童年有阴影，我小时候喜欢火车，去游乐园就想坐火车。进去之后就一直找，还真有一辆小火车，我就坐上去，工作人员还给我系安全带，我就感觉要出事，我往前一看，跑道是上天的，是过山车。\r\n我要下车已经来不及了，跑了一圈之后，平生再没去过游乐园。知道了高处不胜寒，还是在地上呆着吧。\r\n我这公司有个同事比我还熊，去年郊游，在一个公园玩小美人鱼，就是一个人鱼的模型，坐在里面转圈，转的非常快，非常突然。转了三圈，同事就傻逼了，像唐僧被妖精关在山洞里一样，高喊“放我出去！放我出去！”\r\n\r\n大学的时候就有人打电话让我买保险，也是一姑娘。\r\n她说不推销，搞个活动，问我叫啥，哪里人。\r\n我说，东北的。\r\n她说，你废话，我这是东北区公司。\r\n我说，吉林的。\r\n她说，听说你那里的人，男的帅气，女的漂亮。我能介绍一下活动么？\r\n我说，好吧，啥活动。\r\n\r\n后来毕业短暂的做过一段销售知道，那是最基本的话术。\r\n随便你老家哪的，都是人杰地灵。\r\n随便你做什么行业，都是人才辈出的的精英行业。\r\n随便你哪个学校毕业的，都是久仰贵校。\r\n\r\n我有的同事就爱乱取悦客户，一次还和客户说“哎呦，您和潘玮柏是校友吧？”\r\n喝多了吧，内地学校怎么可能和台湾人是校友。\r\n一次我促成一单，约人来公司开户，到约定日期时，经理着急，让我提醒一下。\r\n我跟客户说“今天下午三点，拿多少多少钱，在XXX见面。”\r\n经理说“你是绑匪么？”\r\n\r\n反正那个团队都是精英，一次和一个同事去办事，路上他摔了一跤。\r\n摔之前是穿着一条西裤，站起来就剩一只西裤，一个裤腿摔碎了。\r\n他说“这尼玛的，四十买的裤子就是不行。”\r\n我说“一会儿还见人呢，先买一条吧。不能一半西裤，一半裤衩啊。”\r\n公司还有一个百万宝儿，她总遇见大客户，说要来公司投一百万，弄的经理赶紧开会准备迎接客户，结果第二天那人说姑娘我逗你玩。\r\n还有一次，又出来一个人要投一百万，经理又开会，这次客户真来了，不过说“我其实不是来投资的，想跟你处对象，来见见你。”\r\n客户走后，经理要去厕所，抽面巾纸，一张，两张，三张，六张，八张，拿了一大堆。[图片1]', 0, 0, 1, 1, 0, 1, 1397523634),
(3, 1, '油漆的师傅居然问我“你想当学徒？你看见我贴的广告了？', '走在街上，也总能遇见营销的。\r\n去年我同事说，坐地铁有美女和他搭讪，后来聊天也是推销保险的。\r\n一个女同事要换房子，她说一出门全小区的地产中介都跟着她，要跟她签合同，都赶上保镖了。\r\n我从来没遇见地产行业的推销，以前可能是年纪小，看着不像潜在客户，现在不小了，也没遇见过。\r\n可能是我气质太差，在日企的时候，一次下班遇见路边有人刷油漆，我闻油漆味上瘾，站着陶醉了一会，刷[图片1]待遇没广告上写的那么夸张，不过三四千的没问题，也不累，就是刷刷么。”\r\n\r\n反正在街上拉我的，基本上都是培训班的，最搞笑的是还有减肥的。看不见本人已经皮包骨了么，再减肥就能穿墙了。\r\n我最讨厌的就是美发店的推销，不胜其烦。\r\n去年夏天在北京理工门口，我在等人，一个做美发的，非让我进去，说给我一点建议。\r\n他非常客气，而且我等人也没事，夏天多理发几次也行。\r\n进去后他给我简单说了几句，大概就是我头型不行，应该由他设计一下，今天友情价38。\r\n我说“行。”\r\n他说“你赚了，我不是一般的理发匠，我是造型师，总给明星弄。”然后拿出手机，里面有一张和李晨的合影。\r\n有一个哥们，被骗进来后，听说要花钱，直接跑了，搞笑的是，理发店给他洗头，但是没有冲洗，头发上都是泡沫就出门。\r\n理发后，我给他100。\r\n他不着钱，说“这样，今天我高兴，就60块钱，给你烫头。多便宜啊，平时500多，就是交朋友。”\r\n我说“不烫，不是钱的事儿，我们公司是传统行业，发型普通点就行。”\r\n他说“就10分钟。”\r\n我说“刚才让我进来，你还说就一分钟呢，我得走了，在等人。”', 0, 0, 1, 1, 0, 1, 1397523661),
(4, 1, '大学时对外表还帅在乎，固定去一家理发店。', '关于理发，我同事还在民大附近被骗过，500班长卡，第一次去就做头像花了300多。\r\n第二天没看出来头发长了还是断了，也看不出烫了还是没烫。\r\n我们说“300多去洗洗头，你真有钱。”\r\n[图片1]\r\n那个师傅认识我后，向我推销发蜡，说“每天抹一点，显得自然一些，这韩国进口的，五十成本价给你。”\r\n回去第一次用那发蜡，抹了一些在手上搓，准备抹头发的时候，双手差点没分开，太粘了。\r\n我感觉这发蜡太极品，叫我下铺说“来，我给你造个型？”\r\n下铺刚洗完头，说“弄呗。”\r\n我用了至少十分之一的量在他头上，胡乱的抓了几把，头型被我弄的很乱，有点像七龙珠里的贝吉塔，我说“敢不敢就这样？”\r\n下铺说“我自己来吧。”\r\n他自己把头发抹平，头发特别亮，像黑客帝国时期的基努李维斯，在窗边站了一会儿说“操，你这什么发蜡，这么大的风，我连一根头发都不随风飘扬，你给我弄固体胶了吧？”\r\n然后下铺又去洗头，据说洗了三遍。', 0, 0, 1, 1, 0, 1, 1397523687);

-- --------------------------------------------------------

--
-- 表的结构 `ik_user_online`
--

CREATE TABLE IF NOT EXISTS `ik_user_online` (
  `userid` int(11) NOT NULL,
  `ctime` int(11) NOT NULL,
  UNIQUE KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='在线用户';

-- --------------------------------------------------------

--
-- 表的结构 `ik_user_photo`
--

CREATE TABLE IF NOT EXISTS `ik_user_photo` (
  `photoid` int(11) NOT NULL AUTO_INCREMENT,
  `albumid` int(11) NOT NULL DEFAULT '0' COMMENT '相册ID',
  `userid` int(11) NOT NULL DEFAULT '0',
  `photopath` varchar(255) NOT NULL DEFAULT '' COMMENT '图片路径',
  `photoname` varchar(255) NOT NULL DEFAULT '',
  `photodesc` varchar(255) NOT NULL DEFAULT '',
  `count_view` int(11) NOT NULL DEFAULT '0',
  `count_comment` int(11) NOT NULL DEFAULT '0',
  `isrecommend` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0不推荐1推荐',
  `addtime` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`photoid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- 转存表中的数据 `ik_user_photo`
--

INSERT INTO `ik_user_photo` (`photoid`, `albumid`, `userid`, `photopath`, `photoname`, `photodesc`, `count_view`, `count_comment`, `isrecommend`, `addtime`) VALUES
(1, 1, 1, 'photo/1/1/', '534c85331a670.jpg', '', 0, 0, 0, 1397523763),
(2, 1, 1, 'photo/1/1/', '534c8533a9f71.jpg', '', 0, 0, 0, 1397523764),
(3, 1, 1, 'photo/1/1/', '534c8534c4d61.jpg', '', 0, 0, 0, 1397523765),
(4, 1, 1, 'photo/1/1/', '534c8535ce20c.jpg', '', 0, 0, 0, 1397523766),
(5, 2, 1, 'photo/1/2/', '534c859b5bd2b.jpg', '', 0, 0, 0, 1397523867),
(6, 2, 1, 'photo/1/2/', '534c859bc1643.jpg', '', 0, 0, 0, 1397523868),
(7, 2, 1, 'photo/1/2/', '534c859cdcc03.jpg', '', 0, 0, 0, 1397523869),
(8, 3, 1, 'photo/1/3/', '534c85eb56592.jpg', '', 0, 0, 0, 1397523947),
(9, 3, 1, 'photo/1/3/', '534c85ebc9585.jpg', '', 0, 0, 0, 1397523948),
(10, 3, 1, 'photo/1/3/', '534c85ec4506f.jpg', '', 0, 0, 0, 1397523948),
(11, 4, 1, 'photo/1/4/', '534c860c11781.jpg', '', 0, 0, 0, 1397523980),
(12, 4, 1, 'photo/1/4/', '534c860c49de6.jpg', '', 0, 0, 0, 1397523981);

-- --------------------------------------------------------

--
-- 表的结构 `ik_user_photo_album`
--

CREATE TABLE IF NOT EXISTS `ik_user_photo_album` (
  `albumid` int(11) NOT NULL AUTO_INCREMENT COMMENT '相册ID',
  `userid` int(11) NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '相册路径',
  `albumface` varchar(255) NOT NULL DEFAULT '' COMMENT '相册封面',
  `albumname` char(64) NOT NULL DEFAULT '',
  `albumdesc` varchar(400) NOT NULL DEFAULT '' COMMENT '相册介绍',
  `count_photo` int(11) NOT NULL DEFAULT '0',
  `count_view` int(11) NOT NULL DEFAULT '0',
  `orderid` varchar(20) NOT NULL DEFAULT 'desc' COMMENT '排序desc asc',
  `privacy` tinyint(1) NOT NULL DEFAULT '1' COMMENT '浏览权限:1所有人可见 2仅朋友可见 3仅自己可见',
  `isreply` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否允许回复:0不允许 1允许',
  `isrecommend` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否推荐',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `uptime` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`albumid`),
  KEY `userid` (`userid`),
  KEY `isrecommend` (`isrecommend`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='相册' AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `ik_user_photo_album`
--

INSERT INTO `ik_user_photo_album` (`albumid`, `userid`, `path`, `albumface`, `albumname`, `albumdesc`, `count_photo`, `count_view`, `orderid`, `privacy`, `isreply`, `isrecommend`, `addtime`, `uptime`) VALUES
(1, 1, 'photo/1/1/', '534c85331a670.jpg', '完美程序', '完美程序完美程序', 4, 0, 'desc', 1, 1, 1, 1397523752, 1397523752),
(2, 1, 'photo/1/2/', '534c859b5bd2b.jpg', '美女', '美女美女美女美女', 3, 0, 'desc', 1, 1, 1, 1397523855, 1397523855),
(3, 1, 'photo/1/3/', '534c85eb56592.jpg', '推荐相册后台设置', '推荐相册后台设置推荐相册后台设置', 3, 0, 'desc', 1, 1, 1, 1397523941, 1397523941),
(4, 1, 'photo/1/4/', '534c860c49de6.jpg', '推荐相册', '推荐相册后', 2, 0, 'desc', 1, 1, 1, 1397523974, 1397523974);

-- --------------------------------------------------------

--
-- 表的结构 `ik_user_role`
--

CREATE TABLE IF NOT EXISTS `ik_user_role` (
  `roleid` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `rolename` char(32) NOT NULL DEFAULT '' COMMENT '角色名称',
  `score_start` int(11) NOT NULL DEFAULT '0' COMMENT '积分开始',
  `score_end` int(11) NOT NULL DEFAULT '0' COMMENT '积分结束',
  PRIMARY KEY (`roleid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='角色' AUTO_INCREMENT=18 ;

--
-- 转存表中的数据 `ik_user_role`
--

INSERT INTO `ik_user_role` (`roleid`, `rolename`, `score_start`, `score_end`) VALUES
(1, '列兵', 0, 5000),
(2, '下士', 5000, 20000),
(3, '中士', 20000, 40000),
(4, '上士', 40000, 80000),
(5, '三级准尉', 80000, 160000),
(6, '二级准尉', 160000, 320000),
(7, '一级准尉', 320000, 640000),
(8, '少尉', 640000, 1280000),
(9, '中尉', 1280000, 2560000),
(10, '上尉', 2560000, 5120000),
(11, '少校', 5120000, 10240000),
(12, '中校', 10240000, 20480000),
(13, '上校', 20480000, 40960000),
(14, '准将', 40960000, 81920000),
(15, '少将', 81920000, 123840000),
(16, '中将', 123840000, 327680000),
(17, '上将', 327680000, 0);

-- --------------------------------------------------------

--
-- 表的结构 `ik_user_score`
--

CREATE TABLE IF NOT EXISTS `ik_user_score` (
  `scoreid` int(11) NOT NULL AUTO_INCREMENT COMMENT '积分ID',
  `action` char(64) NOT NULL,
  `actionname` char(64) NOT NULL DEFAULT '' COMMENT '积分名称',
  `score` int(11) NOT NULL DEFAULT '0' COMMENT '积分数',
  `maxnum` int(11) NOT NULL DEFAULT '0' COMMENT '上限',
  PRIMARY KEY (`scoreid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户积分设置表' AUTO_INCREMENT=7 ;

--
-- 转存表中的数据 `ik_user_score`
--

INSERT INTO `ik_user_score` (`scoreid`, `action`, `actionname`, `score`, `maxnum`) VALUES
(1, 'register', '注册', 20, 1),
(2, 'login', '登录', 10, 5),
(3, 'pubtopic', '发布帖子', 10, 10),
(4, 'deltopic', '删除帖子', -10, 10),
(5, 'pubcmt', '发布评论', 5, 10),
(6, 'delcmt', '删除评论', -5, 10);

-- --------------------------------------------------------

--
-- 表的结构 `ik_user_score_log`
--

CREATE TABLE IF NOT EXISTS `ik_user_score_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `uname` varchar(50) NOT NULL,
  `action` varchar(50) NOT NULL,
  `actionname` varchar(50) NOT NULL,
  `score` int(10) NOT NULL,
  `add_time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `ik_user_stat`
--

CREATE TABLE IF NOT EXISTS `ik_user_stat` (
  `uid` int(10) unsigned NOT NULL,
  `action` varchar(20) NOT NULL,
  `num` int(10) unsigned NOT NULL,
  `last_time` int(10) unsigned NOT NULL,
  UNIQUE KEY `uid_type` (`uid`,`action`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ik_videos`
--

CREATE TABLE IF NOT EXISTS `ik_videos` (
  `videoid` int(11) NOT NULL AUTO_INCREMENT COMMENT '视频id',
  `seqid` int(11) NOT NULL DEFAULT '0' COMMENT '顺序id',
  `typeid` int(11) NOT NULL DEFAULT '0' COMMENT '日记ID或帖子id',
  `type` char(64) NOT NULL DEFAULT '0' COMMENT '日记或帖子或其他组件',
  `userid` int(11) NOT NULL DEFAULT '0',
  `url` char(255) NOT NULL DEFAULT '' COMMENT '视频网址',
  `imgurl` char(255) NOT NULL DEFAULT '' COMMENT '视频截图',
  `videourl` char(255) NOT NULL DEFAULT '' COMMENT 'swf地址',
  `title` char(120) NOT NULL DEFAULT '' COMMENT '视频标题',
  `count_view` int(11) NOT NULL DEFAULT '0',
  `addtime` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`videoid`),
  KEY `typeid` (`typeid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `ik_words`
--

CREATE TABLE IF NOT EXISTS `ik_words` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `admin` varchar(50) NOT NULL,
  `find` varchar(255) NOT NULL DEFAULT '' COMMENT '违禁词语',
  `replacement` varchar(255) NOT NULL DEFAULT '' COMMENT '替换词',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='词组过滤' AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
