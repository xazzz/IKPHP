<?php
/*
 * IKPHP 爱客开源社区 @copyright (c) 2012-3000 IKPHP All Rights Reserved 
 * @author 小麦
 * @Email:810578553@qq.com
 * @爱客网 网站升级提醒
 */
namespace Home\Controller;
use Common\Controller\FrontendController;

class NoticeController extends FrontendController {
	public function _initialize() {
		parent::_initialize ();
	}
    //网站提醒
    public function isupdate(){
    	//$ref = $_SERVER['HTTP_REFERER'];
    	//$ip = get_client_ip();
    	$v = I('get.v'); 
    	if($v<IKPHP_VERSION){
    		$html = '<tr><td width="100" style="color:red">发现有新版本：</td><td style="color:red">IKPHP 1.5.4 版本 <a href="http://www.ikphp.cn" target="_blank">[下载升级包]</a></td></tr>';
    		$this->ajaxReturn($html,'JSONP');
    	}else{
    		$this->ajaxReturn('<tr><td width="100">还没有新版本：</td><td>请再等等吧^_^！</td></tr>','JSONP');
    	}
    }
}